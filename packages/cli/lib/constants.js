exports.TEMPLATES_REPO_URL =
	'https://api.github.com/users/oreactjs-templates/repos';

exports.CUSTOM_TEMPLATE = {
	value: 'custom',
	title: 'Custom',
	description: 'Use your own template',
};

exports.FALLBACK_TEMPLATE_OPTIONS = [
	{
		value: 'oreactjs-templates/default',
		title: 'default',
		description: 'The default template for Oreact CLI',
	},
	{
		value: 'oreactjs-templates/typescript',
		title: 'typescript',
		description: 'The default template for Oreact CLI in typescript',
	},
	{
		value: 'oreactjs-templates/material',
		title: 'material',
		description: 'The material design `oreact-cli` template',
	},
	{
		value: 'oreactjs-templates/simple',
		title: 'simple',
		description: 'A simple, minimal "Hello World" template for Oreact CLI',
	},
	{
		value: 'oreactjs-templates/netlify',
		title: 'netlify',
		description: 'A oreactjs and netlify CMS template',
	},
	{
		value: 'oreactjs-templates/widget',
		title: 'widget',
		description: 'Template for a widget to be embedded in another website',
	},
];

exports.TEMPLATES_CACHE_FOLDER = '.cache';
exports.TEMPLATES_CACHE_FILENAME = 'oreact-templates.json';
