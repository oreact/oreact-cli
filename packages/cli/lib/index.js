#!/usr/bin/env node
const envinfo = require('envinfo');
const sade = require('sade');
const notifier = require('update-notifier');
const { error } = require('./util');
const pkg = require('../package');

const ver = process.version;
const min = pkg.engines.node;
if (
	ver
		.substring(1)
		.localeCompare(min.match(/\d+/g).join('.'), 'en', { numeric: true }) === -1
) {
	return error(
		`You are using Node ${ver} but oreact-cli requires Node ${min}. Please upgrade Node to continue!`,
		1
	);
}

// Safe to load async-based funcs
const commands = require('./commands');

// installHooks();
notifier({ pkg }).notify();

process.on('unhandledRejection', err => {
	error(err.stack || err.message);
});

let prog = sade('oreact').version(pkg.version);

prog
	.command('tailwind [command]')
	.describe('Build/Purge tailwind css.')
	.option('--command', 'Specify command to either build or purge', 'command')
	.option('--cwd', 'A directory to use instead of $PWD', '.')
	.option('-v, --verbose', 'Verbose output')
	.action(commands.tailwind);

prog
	.command('start [command]')
	.describe('Start a live-reload server for development and run production')
	.option('--command', 'Specify command to either start for dev or production', 'command')
	.option('--cwd', 'A directory to use instead of $PWD', '.')
	.option('-v, --verbose', 'Verbose output')
	.action(commands.start);

prog
	.command('build [src]')
	.describe('Create a production build')
	.option('--src', 'Specify source directory', 'src')
	.option('--cwd', 'A directory to use instead of $PWD', '.')
	.option('-v, --verbose', 'Verbose output')
	.action(commands.build);

/*
prog
	.command('create [template] [dest]')
	.describe('Create a new application')
	.option('--name', 'The application name')
	.option('--cwd', 'A directory to use instead of $PWD', '.')
	.option('--force', 'Force destination output; will override!', false)
	.option('--install', 'Install dependencies', true)
	.option('--yarn', 'Use `yarn` instead of `npm`', false)
	.option('--git', 'Initialize git repository', false)
	.option('--license', 'License type', 'MIT')
	.option('-v, --verbose', 'Verbose output', false)
	.action(commands.create);

prog
	.command('list')
	.describe('List official templates')
	.action(commands.list);

prog
	.command('watch [src]')
	.describe('Start a live-reload server for development')
	.option('--src', 'Specify source directory', 'src')
	.option('--cwd', 'A directory to use instead of $PWD', '.')
	.option('--esm', 'Builds ES-2015 bundles for your code.', false)
	.option('--clear', 'Clear the console', true)
	.option('--sw', 'Generate and attach a Service Worker', undefined)
	.option('--rhl', 'Enable react hot loader', false)
	.option('--json', 'Generate build stats for bundle analysis')
	.option('--https', 'Run server with HTTPS protocol')
	.option('--key', 'Path to PEM key for custom SSL certificate')
	.option('--cert', 'Path to custom SSL certificate')
	.option('--cacert', 'Path to optional CA certificate override')
	.option('--prerender', 'Pre-render static content on first run')
	.option('--template', 'Path to custom HTML template')
	.option('-c, --config', 'Path to custom CLI config', 'oreact.config.js')
	.option('-H, --host', 'Set server hostname', '0.0.0.0')
	.option('-p, --port', 'Set server port', 8080)
	.option(
		'--prerenderUrls',
		'Path to pre-rendered routes config',
		'prerender-urls.json'
	)
	.action(commands.watch);

prog
	.command('info')
	.describe('Print out debugging information about the local environment')
	.action(() => {
		process.stdout.write('\nEnvironment Info:');
		envinfo
			.run({
				System: ['OS', 'CPU'],
				Binaries: ['Node', 'Yarn', 'npm'],
				Browsers: ['Chrome', 'Edge', 'Firefox', 'Safari'],
				npmPackages: [
					'oreact',
					'oreact-compat',
					'oreact-cli',
					'oreact-router',
					'oreact-render-to-string',
				],
				npmGlobalPackages: ['oreact-cli'],
			})
			.then(info => process.stdout.write(`${info}\n`));
	});*/

prog.parse(process.argv);
