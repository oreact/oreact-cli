const rimraf = require('rimraf');
const { resolve } = require('path');
const { promisify } = require('util');
const { isDir, error } = require('../util');
const spawn = require('cross-spawn-promise');

const toBool = val => val === void 0 || (val === 'false' ? false : val);
const stdio = 'inherit';

module.exports = async function(command, argv) {
	argv.command = command || argv.command;
	// add `default:true`s, `--no-*` disables
	argv.prerender = toBool(argv.prerender);
	argv.production = toBool(argv.production);

	let cwd = resolve(argv.cwd);
	let modules = resolve(cwd, 'node_modules');

	if (!isDir(modules)) {
		return error(
			'No `node_modules` found! Please run `npm install` before continuing.',
			1
		);
	}

	let cmd = ``, args = [];
	switch (argv.command) {
		case 'dev':
			cmd = `oreact tailwind build && ${resolve(cwd, 'node_modules/.bin/razzle')} start`;
			break;
		case 'prod':
			cmd = `${require.resolve('../../node_modules/.bin/cross-env')} NODE_ENV=production node -r ${require.resolve('../../node_modules/dotenv/config')} ${resolve(cwd, 'build/server.js')}`;
			break;
	}
	console.log(cmd);
	await spawn(cmd, [...args], { shell: true, cwd, stdio }).catch((error) => {
		console.error('Failed!');
		console.error('exit status:', error.exitStatus);
		console.log(error);
		//console.error('stderr:', error.stderr.toString())
	});

};
