const PRERENDER_DATA_FILE_NAME = 'oreact_prerender_data.json';

module.exports = {
	PRERENDER_DATA_FILE_NAME,
};
